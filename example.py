from tab_user_mgmt import TabUserManagement, TableauSiteRoles

# Update the following with your Tableau Server details
tableau_base_url = 'https://tableau.my-company.com'
tableau_api_version = '3.4' # Find your REST API version at https://onlinehelp.tableau.com/current/api/rest_api/en-us/REST/rest_api_concepts_versions.htm#version-availability
admin_username = 'server_admin_username'
admin_password = 'server_admin_password'

# Intialize and sign into to Tableau Server
tum = TabUserManagement(tableau_base_url, tableau_api_version)
print('\nSigning into Tableau Server REST API as {}\n'.format(admin_username))
tum.sign_in(admin_username, admin_password)

# Add a new user
new_user = tum.add_user('my_new_user', TableauSiteRoles.Viewer, full_name='New User', email='new_user@mycompany.com', password='password123')
message = (
    'Successfully added a new user with the following details:\n'
    'ID: {}\n'
    'Username: {}\n'
    'Full Name: {}\n'
    'Email: {}\n'
).format(
    new_user['id'],
    new_user['name'],
    new_user['fullName'],
    new_user['email']
)
print(message)

# Remove a user
removed_user = tum.remove_user('my_new_user')
message = (
    'Successfully removed the following user:\n'
    'ID: {}\n'
    'Username: {}\n'
).format(
    removed_user['removedUser']['id'],
    removed_user['removedUser']['name']
)
print(message)

# Sign out of Tableau Server
print('Signing out of Tableau Server REST API')
tum.sign_out()