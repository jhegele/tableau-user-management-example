"""
helpers.py

Helper classes and functions used throughout tab_user_mgmt
"""

class TableauAPIError(Exception):
    """Custom error class for API-specific error reporting"""
    pass

class TableauUserManagementError(Exception):
    """Custom error class for class-specific error reporting"""
    pass

class TableauSiteRoles(object):
    """Enum of valid site roles"""
    Creator = 'Creator'
    Explorer = 'Explorer'
    ExplorerCanPublish = 'ExplorerCanPublish'
    SiteAdminstratorExplorer = 'SiteAdministratorExplorer'
    SiteAdministratorCreator = 'SiteAdministratorCreator'
    Unlicensed = 'Unlicensed'
    Viewer = 'Viewer'

def api_error_message(error_json):
    """Build a custom error message"""
    return (
        'Error Signing Out\n'
        'Code: {}\n'
        'Summary: {}\n'
        'Detail: {}\n'
    ).format(
        error_json['error']['code'],
        error_json['error']['summary'],
        error_json['error']['detail']
    )